/*
-- No. 1
*/
console.log('-----------');
console.log('Soal No. 1'); 
console.log('-----------');

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
console.log(daftarHewan.sort());

console.log('\n'); //membuat garis baru

/*
-- No. 2
*/
console.log('-----------');
console.log('Soal No. 2'); 
console.log('-----------');

function introduce(date) {
  return "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby + "!";
}
var data = {
	name : "John", 
	age : 30, 
	address : "Jalan Pelesiran", 
	hobby : "Gaming" 
}

var perkenalan = introduce(data);
console.log(perkenalan);

console.log('\n'); //membuat garis baru

/*
-- No. 3
*/
console.log('-----------');
console.log('Soal No. 3'); 
console.log('-----------');

function hitung_huruf_vokal(hitung_1, hitung_2)
{
	return hitung_1.length;
	return hitung_2.length;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Ibrahim");

console.log("Muhammad jumlah huruf : " + hitung_1) // 8
console.log("Ibrahim jumlah huruf : " + hitung_2) // 7

console.log('\n'); //membuat garis baru

/*
-- No. 4
*/
console.log('-----------');
console.log('Soal No. 4'); 
console.log('-----------');

function hitung(angka)
{
	return angka%13
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
