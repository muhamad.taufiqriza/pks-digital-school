<?php
abstract class Hewan {
  public $name;
  public $darah = 50;
  public $kaki;
  public $keahlian;
  public function __construct($name, $darah = 50, $kaki, $keahlian) {
    $this->name = $name;
    $this->darah = $darah;
    $this->kaki = $kaki;
    $this->keahlian = $keahlian;
  }
  abstract public function atraksi();
}

abstract class Fight {
    public $attackPower;
    public $defencePower;
    public function __construct($attackPower, $defencePower) {
      $this->attackPower = $attackPower;
      $this->defencePower = $defencePower;
    }
    abstract public function serang();
    abstract public function diserang();
  }

class Elang extends Hewan {
    public static
    public static function atraksi() : string {
      return "Namaw Hewan $this->name!" . "Keahlian $kahlian";
    }
  }
  
  class Harimau extends Hewan {
    public function intro() : string {
      return "Proud to be Swedish! I'm a $this->name!";
    }
  }
  
  $audi = new audi("Audi");
  echo $audi->intro();
  echo "<br>";
  
  $volvo = new volvo("Volvo");
  echo $volvo->intro();
  echo "<br>";
  
  $citroen = new citroen("Citroen");
  echo $citroen->intro();
  ?>
  <!-- Maap bmasih bingungg -->
